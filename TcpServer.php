<?php

	Class TcpServer
	{
		public $prot;
		public $ip;
		public function __construct($ip='0.0.0.0',$port=3000)
		{
			$this->port = $port;
			$this->ip = $ip;
			$this->serv = new swoole_server($this->ip,$this->port);
			$this->set();
			$this->listen();
		}

		public function set()
		{
			$this->serv->set=[
				'worker_num' => 4,
				'task_worker_num' =>4,
			];
		}

		public function run()
		{
			$this->serv->start();
		}

		public function listen()
		{
			$this->serv->on('workerStart',[$this,'workerStart']);
			$this->serv->on('connect',[$this,'connect']);
			$this->serv->on('receive',[$this,'receive']);
			$this->serv->on('close',[$this,'close']);
		}

		public function workerStart($serv)
		{
			
		}

		public function connect($serv,$fd)
		{
			echo "Serv: {$fd} connect\n";
		}

		public function receive()
		{

		}

		public function close()
		{
			echo "Serv: {$fd} close\n";
		}
	}

	class Tcp extends TcpServer
	{
		public function receive($serv,$fd,$from_id,$data)
		{
			$data = 'Serv: '.$fd.':'.$data;
			foreach($serv->connections as $cli)
				if($cli!==$fd)$serv->send($cli,$data);
		}
	}

	//(new Tcp)->run();